# pylint: disable=C0301,R0903,C0103,R0914,W0105

'''Recommender'''

import os
import socket
# import random
import json
import requests  # for calling /anime-api
import math

from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)

if os.environ.get('db_conn') and os.environ.get('db_name'):
    print('\n\n\n\nHello there\n\n\n\n')
    app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get('db_conn') + '/' + os.environ.get('db_name')
else:
    app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql+psycopg2://postgres:example@localhost:5433/test_db"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {"pool_size": 100, "pool_recycle": 280}
db = SQLAlchemy(app)

CORS(app)


class Logs(db.Model):
    ''' logs table'''

    __tablename__ = 'logs'

    user_id = db.Column(db.String(), primary_key=True, nullable=False)
    anime_id = db.Column(db.String(), primary_key=True, nullable=False)
    score = db.Column(db.Integer, primary_key=True, nullable=False)
    date_time = db.Column(db.String(), primary_key=True, nullable=False)

    def __init__(self, user_id, anime_id, score, date_time):
        self.user_id = user_id
        self.anime_id = anime_id
        self.score = score
        self.date_time = date_time

    def to_dict(self):
        '''Return dictionary representation'''

        return {
            'user_id': self.user_id,
            'anime_id': self.anime_id,
            'score': self.score,
            'date_time': self.date_time
        }


class UtilityMatrix(db.Model):
    '''utility_matrix table'''

    __tablename__ = 'utility_matrix'

    user_id = db.Column(db.String(), primary_key=True, nullable=False)
    user_preferences = db.Column(db.String(), nullable=False)

    def __init__(self, user_id, user_preferences):
        self.user_id = user_id
        self.user_preferences = user_preferences

    def to_dict(self):
        '''Return dictionary representation'''

        return {
            'user_id': self.user_id,
            'user_preferences': self.user_preferences
        }


def compute_similarity(target_user_preferences, other_user_preferences):
    '''Helper functions'''

    score = 0
    for key in target_user_preferences.keys():
        target_value = target_user_preferences[key]
        other_value = other_user_preferences[key]
        score += target_value * other_value
    return score


@app.route("/recommendations/all-user-preferences")
def get_all_user_preferences():
    '''Return all user preferences'''

    all_user_preferences = db.session.scalars(db.select(UtilityMatrix)).all()
    if len(all_user_preferences) != 0:
        return jsonify({"data": {"records": [
            record.to_dict() for record in all_user_preferences
        ]}}), 200
    return jsonify({"message": "There are no records."}), 404


# content-based filtering
@app.route("/recommendations/content/<string:user_id>:<int:n>")
def get_content_recommendations(user_id, n):
    '''Return content-based recommendations'''

    record = db.session.scalars(
        db.select(UtilityMatrix).filter_by(user_id=user_id).limit(1)
    ).first()

    content_recommendations = None

    if not record:

        response = None

        if not os.environ.get('stage') or os.environ.get('stage') == 'dev':
            response = ['new user : n x test_anime_id']
        elif os.environ.get('stage') == 'prod':
            anime_api_result = requests.get(os.environ.get('anime_host') + '/anime/popularity/' + str(n))
            response = [0] * n
            for i in range(n):
                response[i] = anime_api_result.json()[i]['id']

        return {"data": {
                "user_id": user_id,
                "content_recommendations": response
                }}, 200

    user_preferences = json.loads(record.user_preferences)

    sorted_user_preferences = sorted(user_preferences.items(), key=lambda x: x[1])[::-1]

    count = 0

    m = len(sorted_user_preferences)
    weights = [[sorted_user_preferences[i][0], 0] for i in range(m)]
    total = sum(sorted_user_preferences[x][1] for x in range(m))

    if total == 0:

        response = None

        if not os.environ.get('stage') or os.environ.get('stage') == 'dev':
            response = ['no preferences : n x test_anime_id']
        elif os.environ.get('stage') == 'prod':
            anime_api_result = requests.get(os.environ.get('anime_host') + '/anime/popularity/' + str(n))
            response = [0] * n
            for i in range(n):
                response[i] = anime_api_result.json()[i]['id']

        return {"data": {
                "user_id": user_id,
                "content_recommendations": response
                }}, 200

    for i in range(m):

        weights[i][1] = math.ceil((sorted_user_preferences[i][1] / total) * n)

        count += math.ceil((sorted_user_preferences[i][1] / total) * n)

        if count > n:
            weights[i][1] -= (count - n)
            break

    content_recommendations = []
    for i in range(m):

        if not weights[i][1]:
            break

        response = None

        if not os.environ.get('stage') or os.environ.get('stage') == 'dev':
            response = ['test_anime_id*' + str(weights[i][1]) + str(weights)]
        elif os.environ.get('stage') == 'prod':
            genre = weights[i][0]
            # anime_api_result = requests.get(os.environ.get('anime_host') + '/anime/genre?genre=' + genre + '&n=' + str(weights[i][1]))
            anime_api_result = requests.get(os.environ.get('anime_host') + '/anime/genre?genre=' + genre)
            response = []
            for j in range(len(anime_api_result.json())):
                if anime_api_result.json()[j]['anime_id'] not in content_recommendations and len(response) < weights[i][1]:
                    response += [anime_api_result.json()[j]['anime_id']]

        content_recommendations += response

    # random.shuffle(content_recommendations)

    return jsonify({"data": {
                    "user_id": user_id,
                    "content_recommendations": content_recommendations
                    }}), 200


@app.route("/recommendations/collaborative/<string:user_id>:<int:n>")
def get_collaborative_recommendations(user_id, n):
    '''Return collaborative recommendations'''

    all_records = db.session.scalars(db.select(UtilityMatrix)).all()
    target_record = db.session.scalars(
        db.select(UtilityMatrix).filter_by(user_id=user_id).limit(1)
    ).first()

    if not target_record:

        response = None

        if not os.environ.get('stage') or os.environ.get('stage') == 'dev':
            response = ['new user : n x test_anime_id']
        elif os.environ.get('stage') == 'prod':
            anime_api_result = requests.get(os.environ.get('anime_host') + '/anime/popularity/' + str(n))
            response = [0] * n
            for i in range(n):
                response[i] = anime_api_result.json()[i]['id']

        return {"data": {
                "user_id": user_id,
                "collaborative_recommendations": response
                }}, 200

    target_user_id = user_id
    target_user_preferences = json.loads(target_record.user_preferences)

    # print(type(target_user_preferences))
    scores = {}

    for record in all_records:
        other_user_id = record.user_id
        other_user_preferences = json.loads(record.user_preferences)

        if other_user_id != target_user_id:
            score = compute_similarity(target_user_preferences, other_user_preferences)
            scores[other_user_id] = score

    sorted_similar_users = sorted(scores.items(), key=lambda x: x[1])[::-1]

    count = 0

    m = len(sorted_similar_users)
    weights = [[sorted_similar_users[i][0], 0] for i in range(m)]
    total = sum(sorted_similar_users[x][1] for x in range(m))

    if total == 0:
        response = None

        if not os.environ.get('stage') or os.environ.get('stage') == 'dev':
            response = ['no preferences : n x test_anime_id']
        elif os.environ.get('stage') == 'prod':
            anime_api_result = requests.get(os.environ.get('anime_host') + '/anime/popularity/' + str(n))
            response = [0] * n
            for i in range(n):
                response[i] = anime_api_result.json()[i]['id']

        return {"data": {
                "user_id": user_id,
                "collaborative_recommendations": response
                }}, 200

    for i in range(m):

        weights[i][1] = math.ceil((sorted_similar_users[i][1] / total) * n)

        count += math.ceil((sorted_similar_users[i][1] / total) * n)

        if count > n:
            weights[i][1] -= (count - n)
            break

    collaborative_recommendations = []

    for i in range(m):

        if weights[i][0] == user_id:
            continue

        if not weights[i][1]:
            break

        other_user_id = weights[i][0]
        interactions = db.session.scalars(
            db.select(Logs).filter_by(user_id=other_user_id)
        ).all()
        other_user_animes = list(set([int(interactions[j].to_dict()['anime_id']) for j in range(len(interactions))]))

        unique = []
        for anime in other_user_animes:
            if anime not in collaborative_recommendations:
                unique += [anime]

        if len(unique) > weights[i][1]:
            unique = unique[:weights[i][1]]

        collaborative_recommendations += unique

    # random.shuffle(collaborative_recommendations)
    return jsonify({"data": {
                    "user_id": user_id,
                    "collaborative_recommendations": collaborative_recommendations
                    }}), 200


@app.route("/health")
def health_check():
    '''Check if the endpoint is healthy'''

    hostname = socket.gethostname()
    local_ip = socket.gethostbyname(hostname)
    return (
        jsonify(
            {
                "message": "Service is healthy.",
                "service:": "recommender",
                "ip_address": local_ip,
            }
        ),
        200,
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001, debug=True)


'''
# Test Script

class TestORM(db.Model):
    __tablename__ = 'test_table'
    id = db.Column(db.String(10), primary_key=True)
    message = db.Column(db.String(64), nullable=False)

    def __init__(self, id, message):
        self.id = id
        self.message = message

    def to_dict(self):
        return {
            "id": self.id,
            "message": self.message,
        }

@app.route("/test-GET-ALL")
def test_get_all():
    test_list = db.session.scalars(db.select(TestORM)).all()
    if len(test_list) != 0:
        return jsonify({"data": {"records": [record.to_dict() for record in test_list]}}), 200
    return jsonify({"message": "There are no records."}), 404
'''
