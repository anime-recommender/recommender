import json
import pytest


def call(client, path, method='GET', body=None):
    mimetype = 'application/json'
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }

    if method == 'POST':
        response = client.post(path, data=json.dumps(body), headers=headers)
    elif method == 'PUT':
        response = client.put(path, data=json.dumps(body), headers=headers)
    elif method == 'PATCH':
        response = client.patch(path, data=json.dumps(body), headers=headers)
    elif method == 'DELETE':
        response = client.delete(path)
    else:
        response = client.get(path)

    return {
        "json": json.loads(response.data.decode('utf-8')),
        "code": response.status_code
    }

@pytest.mark.dependency()
def test_health(client):
    result = call(client, 'health')
    assert result['code'] == 200


@pytest.mark.dependency()
def test_get_all_user_preferences(client):
    result = call(client, '/recommendations/all-user-preferences')
    assert result['code'] == 200
    assert result['json']['data']['records'] == [
        {
            "user_id": "1",
            "user_preferences": "{\"Romance\": 0, \"Sports\": 0, \"Mystery\": 0, \"Psychological\": 0, \"Music\": 0, \"Fantasy\": 0, \"Slice of Life\": 0, \"Mecha\": 0, \"Horror\": 0, \"Drama\": 0, \"Action\": 0, \"Supernatural\": 0, \"Thriller\": 0, \"Sci-Fi\": 0, \"Comedy\": 0, \"Ecchi\": 0, \"Adventure\": 0}"
        }
    ]


@pytest.mark.dependency()
def test_get_all_user_preferences_no_users(client_clear_utility_matrix):
    result = call(client_clear_utility_matrix, '/recommendations/all-user-preferences')
    assert result['code'] == 404
    assert result['json'] == {
        "message": "There are no records."
    }


@pytest.mark.dependency()
def test_get_content_reco_new_user(client):
    result = call(client, '/recommendations/content/101:1')
    assert result['code'] == 200
    assert result['json']['data']['content_recommendations'] == [
	    "new user : n x test_anime_id"
    ]


@pytest.mark.dependency()
def test_get_content_reco_no_preferences(client):
    result = call(client, '/recommendations/content/1:1')
    assert result['code'] == 200
    assert result['json']['data']['content_recommendations'] == [
	    "no preferences : n x test_anime_id"
    ]

@pytest.mark.dependency()
def test_get_content_reco_from_engine(client_content_reco):
    result = call(client_content_reco, '/recommendations/content/2:1')
    assert result['code'] == 200
    assert result['json']['data'] == {
		"content_recommendations": [
			"test_anime_id*1[['Romance', 1], ['Adventure', 0], ['Ecchi', 0], ['Comedy', 0], ['Sci-Fi', 0], ['Thriller', 0], ['Supernatural', 0], ['Action', 0], ['Drama', 0], ['Horror', 0], ['Mecha', 0], ['Slice of Life', 0], ['Fantasy', 0], ['Music', 0], ['Psychological', 0], ['Mystery', 0], ['Sports', 0]]"
		],
		"user_id": "2"
	}


@pytest.mark.dependency()
def test_get_collaborative_reco_new_user(client):
    result = call(client, '/recommendations/collaborative/101:1')
    assert result['code'] == 200
    assert result['json']['data']['collaborative_recommendations'] == [
	    "new user : n x test_anime_id"
    ]


@pytest.mark.dependency()
def test_get_collaborative_reco_no_preferences(client):
    result = call(client, '/recommendations/collaborative/1:1')
    assert result['code'] == 200
    assert result['json']['data']['collaborative_recommendations'] == [
	    "no preferences : n x test_anime_id"
    ]


@pytest.mark.dependency()
def test_get_collaborative_reco_from_engine(client_collaborative_reco):
    result = call(client_collaborative_reco, '/recommendations/collaborative/2:5')
    assert result['code'] == 200
    assert result['json']['data'] == {
        "collaborative_recommendations": [
			8,
			6,
			7,
			1,
			2
		],
		"user_id": "2"
	}


'''
# Test Script

@pytest.mark.dependency()
def test_get_all(client):
    result = call(client, 'test-GET-ALL')
    assert result['code'] == 200
    assert result['json']['data']['records'] == [
        {
            "id": 1,
            "message": "Hello World"
        }
    ]
'''
