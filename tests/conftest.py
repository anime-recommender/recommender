import pytest


@pytest.fixture
def client():
    from src import app

    app.app.config['TESTING'] = True

    with app.app.app_context():

        with app.db.engine.begin() as connection:

            from sqlalchemy import text

            connection.execute(text('DROP TABLE IF EXISTS utility_matrix;'))

            connection.execute(text('''
                CREATE TABLE IF NOT EXISTS utility_matrix (
                    user_id VARCHAR(255) NOT NULL PRIMARY KEY,
                    user_preferences TEXT NOT NULL
                );
            '''))

            connection.execute(text('''
                INSERT INTO utility_matrix (user_id, user_preferences) 
                    VALUES ('1', '{"Romance": 0, "Sports": 0, "Mystery": 0, "Psychological": 0, "Music": 0, "Fantasy": 0, "Slice of Life": 0, "Mecha": 0, "Horror": 0, "Drama": 0, "Action": 0, "Supernatural": 0, "Thriller": 0, "Sci-Fi": 0, "Comedy": 0, "Ecchi": 0, "Adventure": 0}');
            '''))



    return app.app.test_client()

@pytest.fixture
def client_clear_utility_matrix():
    from src import app

    app.app.config['TESTING'] = True

    with app.app.app_context():

        with app.db.engine.begin() as connection:

            from sqlalchemy import text

            connection.execute(text('DROP TABLE IF EXISTS utility_matrix;'))

            connection.execute(text('''
                CREATE TABLE IF NOT EXISTS utility_matrix (
                    user_id VARCHAR(255) NOT NULL PRIMARY KEY,
                    user_preferences TEXT NOT NULL
                );
            '''))

    return app.app.test_client()

@pytest.fixture
def client_content_reco():
    from src import app

    app.app.config['TESTING'] = True

    with app.app.app_context():

        with app.db.engine.begin() as connection:

            from sqlalchemy import text

            connection.execute(text('DROP TABLE IF EXISTS utility_matrix;'))

            connection.execute(text('''
                CREATE TABLE IF NOT EXISTS utility_matrix (
                    user_id VARCHAR(255) NOT NULL PRIMARY KEY,
                    user_preferences TEXT NOT NULL
                );
            '''))

            connection.execute(text('''
                INSERT INTO utility_matrix (user_id, user_preferences) 
                    VALUES ('2', '{"Romance": 1, "Sports": 0, "Mystery": 0, "Psychological": 0, "Music": 0, "Fantasy": 0, "Slice of Life": 0, "Mecha": 0, "Horror": 0, "Drama": 0, "Action": 0, "Supernatural": 0, "Thriller": 0, "Sci-Fi": 0, "Comedy": 0, "Ecchi": 0, "Adventure": 0}');
            '''))

    return app.app.test_client()

@pytest.fixture
def client_collaborative_reco():
    from src import app

    app.app.config['TESTING'] = True

    with app.app.app_context():

        with app.db.engine.begin() as connection:

            from sqlalchemy import text

            connection.execute(text('DROP TABLE IF EXISTS utility_matrix;'))

            connection.execute(text('''
                CREATE TABLE IF NOT EXISTS utility_matrix (
                    user_id VARCHAR(255) NOT NULL PRIMARY KEY,
                    user_preferences TEXT NOT NULL
                );
            '''))

            connection.execute(text('''
                INSERT INTO utility_matrix (user_id, user_preferences) 
                    VALUES ('1', '{"Romance": 1, "Sports": 0, "Mystery": 1, "Psychological": 0, "Music": 0, "Fantasy": 0, "Slice of Life": 0, "Mecha": 0, "Horror": 0, "Drama": 0, "Action": 0, "Supernatural": 0, "Thriller": 0, "Sci-Fi": 0, "Comedy": 0, "Ecchi": 0, "Adventure": 0}');
            '''))

            connection.execute(text('''
                INSERT INTO utility_matrix (user_id, user_preferences) 
                    VALUES ('2', '{"Romance": 1, "Sports": 1, "Mystery": 1, "Psychological": 0, "Music": 0, "Fantasy": 0, "Slice of Life": 0, "Mecha": 0, "Horror": 0, "Drama": 0, "Action": 0, "Supernatural": 0, "Thriller": 0, "Sci-Fi": 0, "Comedy": 0, "Ecchi": 0, "Adventure": 0}');
            '''))

            connection.execute(text('''
                INSERT INTO utility_matrix (user_id, user_preferences) 
                    VALUES ('3', '{"Romance": 1, "Sports": 1, "Mystery": 1, "Psychological": 0, "Music": 0, "Fantasy": 0, "Slice of Life": 0, "Mecha": 0, "Horror": 0, "Drama": 0, "Action": 0, "Supernatural": 0, "Thriller": 0, "Sci-Fi": 0, "Comedy": 0, "Ecchi": 0, "Adventure": 0}');
            '''))

            connection.execute(text('DROP TABLE IF EXISTS logs;'))

            connection.execute(text('''
                CREATE TABLE IF NOT EXISTS logs (
                    user_id VARCHAR(255) NOT NULL,
                    anime_id VARCHAR(255) NOT NULL,
                    score INT,
                    date_time VARCHAR(255) NOT NULL,
                    
                    PRIMARY KEY (user_id, anime_id, score, date_time)
                );
            '''))

            # a001 > Romance
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('1', '1', 1, '2023-10-24 14:30:00 +0800 +08');
            '''))

            # a002 > Mystery
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('1', '2', 1, '2023-10-24 14:31:00 +0800 +08');
            '''))

            # a003 > Romance
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('2', '3', 1, '2023-10-24 14:32:00 +0800 +08');
            '''))

            # a004 > Sports
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('2', '4', 1, '2023-10-24 14:33:00 +0800 +08');
            '''))

            # a005 > Mystery
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('2', '5', 1, '2023-10-24 14:34:00 +0800 +08');
            '''))

            # a006 > Romance
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('3', '6', 1, '2023-10-24 14:35:00 +0800 +08');
            '''))

            # a007 > Sports
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('3', '7', 1, '2023-10-24 14:36:00 +0800 +08');
            '''))

            # a008 > Mystery
            connection.execute(text('''
                INSERT INTO logs (user_id, anime_id, score, date_time) 
                    VALUES ('3', '8', 1, '2023-10-24 14:37:00 +0800 +08');
            '''))

    return app.app.test_client()